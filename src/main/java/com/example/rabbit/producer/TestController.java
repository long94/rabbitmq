package com.example.rabbit.producer;

/**
 * Sử dụng gửi và nhận message luôn trên 1 queue
 */
//@RestController
//@RequestMapping("/test")
//public class TestRabbitController {
//
//    @Autowired
//    RabbitTemplate rabbitTemplate;
//
//    @GetMapping("/send-message")
//    public ResponseEntity<Object> sendMessage() {
//        Object o = rabbitTemplate.convertSendAndReceive("test_sr", "message");
//        System.out.println(o);
//        return new ResponseEntity<>("OK", HttpStatus.OK);
//    }
//
//    @RabbitHandler
//    @RabbitListener(queues = "test_sr")
//    public String receiveMessage(Message message) {
//        String data = new String(message.getBody());
//        System.out.println(data);
//        return data;
//    }
//}
